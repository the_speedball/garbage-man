from garbage_man.filler import multiple_of


def test_multiple_of_multiple():
    assert not multiple_of(4) % 4
