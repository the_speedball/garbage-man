# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

setup(
    name='garbage_man',
    version='0.0.1',
    packages=find_packages(),
    long_description='',
    include_package_data=True,
    zip_safe=True,
    description='',
    author='Michał Klich',
    author_email='michal@michalklich.com',
    url='https://gitlab.com/the_speedball/garbage-man',
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    license='MIT',
    classifiers=['Development Status :: 4 - Beta', 'Environment :: Console',
                 'License :: OSI Approved :: MIT License',
                 'Operating System :: OS Independent',
                 'Programming Language :: Python :: 2',
                 'Programming Language :: Python :: 2.7',
                 'Programming Language :: Python :: 3',
                 'Programming Language :: Python :: 3.4',
                 'Programming Language :: Python :: 3.5',
                 'Topic :: Software Development :: Build Tools'])
