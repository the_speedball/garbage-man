"""Module with functions generating basic JSON types.
According to the rules of json schema validation.

"""
import random

# taken from https://tools.ietf.org/html/draft-wright-json-schema-validation-00#section-5
VALIDATION_KEYWORDS = (
    'multipleOf', 'maximum', 'exclusiveMaximum', 'mininum', 'exclusiveMinimum',
    'maxLength', 'minLength', 'pattern', 'additionalItems', 'maxItems',
    'minItems', 'uniqueItems', 'maxProperties', 'minProperties', 'required',
    'properties', 'patternProperties', 'additionalProperties', 'dependecies',
    'enum', 'type', 'allOf', 'anyOf', 'oneOf', 'not', 'definitions')


def multiple_of(of):
    return of * random.randrange(1, 1000)


def null():
    # TODO: is this even required?
    return None


def boolean():
    return random.choice([True, False])


def number():
    pass


def string():
    """Should generate strings, not characters.
    """
    pass


def array():
    pass


def object():
    pass
